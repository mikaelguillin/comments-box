import * as types from '../constants/ActionTypes';

export function addComment(author, email, content) {
	return {
		type: types.ADD_COMMENT,
		author,
		email,
		content,
		creationDate: Date.now()
	};
}