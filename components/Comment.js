import React, { Component, PropTypes } from 'react';

export default class Comment extends Component {
	render() {
		return (
			<li>
				<div className={'author'}>
					<span className={'author-name'}>{this.props.author}</span>
					<span className={'author-email'}>email : {this.props.email}</span>
				</div>
				<div className={'content'}>{this.props.content}</div>
			</li>
    	);
	}
}

Comment.propTypes = {
	author: PropTypes.string.isRequired,
	email: PropTypes.string.isRequired,
	content: PropTypes.string.isRequired
};