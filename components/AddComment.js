import React, { findDOMNode, Component, PropTypes } from 'react';
import { isEmpty, isValidEmail } from '../utils/validation';

export default class AddComment extends Component {

	handleClick() {
		const inputAuthor = findDOMNode(this.refs.inputAuthor);
		const inputEmail = findDOMNode(this.refs.inputEmail);
		const textarea = findDOMNode(this.refs.textarea);

		const author = inputAuthor.value.trim();
		const email = inputEmail.value.trim();
		const content = textarea.value.trim();

		if (!isEmpty(author) && !isEmpty(email) && isValidEmail(email) && !isEmpty(content))
		{
			this.props.onAddClick(author, email, content);
		}

		inputAuthor.value = '';
		inputEmail.value = '';
		textarea.value = '';
	}

	render() {
		return (
			<div id="comments-form">
				<div className={"row-form"}>
					<label>Name :</label>
					<input type="text" ref="inputAuthor" />
				</div>
				<div className={"row-form"}>
					<label>Email :</label>
					<input type="text" ref="inputEmail" />
				</div>
				<div className={"row-form"}>
					<label>Comment :</label>
					<textarea ref="textarea"></textarea>
				</div>
				<div className={"row-form"}>
					<button onClick={e => this.handleClick(e)}>
						Add a comment
					</button>
				</div>
      		</div>
    	);
	}
}

AddComment.propTypes = {
	onAddClick: PropTypes.func.isRequired
};