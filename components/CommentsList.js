import React, { Component, PropTypes } from 'react';
import Comment from './Comment';

export default class CommentsList extends Component {
	render() {
		return (
			<ol id='comments-list'>
				{this.props.comments.map((comment, index) =>
					<Comment {...comment} key={index} />
    			)}
      		</ol>
    	);
	}
}

CommentsList.propTypes = {
	comments: PropTypes.arrayOf(PropTypes.shape({
		author: PropTypes.string.isRequired,
		email: PropTypes.string.isRequired,
		content: PropTypes.string.isRequired,
	}).isRequired).isRequired
};