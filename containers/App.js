import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addComment } from '../actions/comments';
import AddComment from '../components/AddComment';
import CommentsList from '../components/CommentsList';

class App extends Component {
	render() {
		const { dispatch, comments } = this.props;

		return (
			<div>
				<CommentsList
					comments = {comments} />

				<AddComment
					onAddClick = {(author, email, content) =>
					dispatch(addComment(author, email, content))
				} />
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		comments: state.comments
	};
}

export default connect(mapStateToProps)(App);