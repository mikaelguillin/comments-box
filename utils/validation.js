export const isEmpty = value => value === undefined || value === null || value === '';

export function isValidEmail(value) {

	if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
		return false;
	}

	return true;
}