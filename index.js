import React from 'react';
import {compose, createStore} from 'redux';
import persistState from 'redux-localstorage';
import { Provider } from 'react-redux';
import App from './containers/App';
import commentsApp from './reducers/comments';
import './sass/style.scss';

const createPersistentStore = compose(
  persistState()
)(createStore);

const store = createPersistentStore(commentsApp);

const commentsBox = document.getElementById('comments-box');

React.render(
	<Provider store={store}>
		{() => <App />}
	</Provider>,
	commentsBox
);
