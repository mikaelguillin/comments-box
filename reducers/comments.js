import { ADD_COMMENT } from '../constants/ActionTypes';

const initialState = {
	comments: [
	{
		author: 'John Doe',
		email: 'john@doe.com',
		content: 'Hello, this is my first comment !',
		creationDate: Date.now()
	}]
};

function comments(state = initialState, action) {

	switch (action.type) {
		case ADD_COMMENT:
			return [...state, {
				author: action.author,
      			email: action.email,
      			content: action.content,
      			creationDate: action.creationDate
    		}];
		default:
		  	return state;
	}
}

export default function commentsApp(state = initialState, action) {
	return {
		comments: comments(state.comments, action)
	};
}